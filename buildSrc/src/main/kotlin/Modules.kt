object Modules {
    const val APP = ":app"

    const val CORE = ":core"
    const val FEATURE_SPLASH = ":feature:splash"
    const val FEATURE_AUTH = ":feature:auth"
    const val FEATURE_MAIN = ":feature:main"
    const val FEATURE_PROFILE_UNKNOWN = ":feature:unknown"
    const val FEATURE_PROFILE_KNOWN = ":feature:known"
    const val FEATURE_PROFILE = ":feature:profile"
    const val FEATURE_VACANCIES = ":feature:vacancies"
    const val FEATURE_SPECIFICATION = ":feature:specification"

    const val MEMES_MVVM = ":memes:mvvm"
    const val MEMES_BENDERADAPTER = ":memes:benderadapter"
    const val MEMES_UTILS = ":memes:utils"
    const val MEMES_DATABINDING = ":memes:databinding"
    const val MEMES_NAVIGATION = ":memes:navigation"
}