object Config {

    const val APPLICATION_ID = "com.urbanist.template"

    const val TARGET_SDK = 28
    const val COMPILE_SDK = 28
    const val MIN_SDK = 19

    const val CODE = 2
    const val NAME = "1.1"

    const val BUILD_TYPE_RELEASE = "release"
    const val BUILD_TYPE_DEBUG = "debug"

    const val CONFIG_FIELD_BACKEND_ENDPOINT = "BACKEND_ENDPOINT"
    const val BACKEND_ENDPOINT_DEV = "\"http://206.81.5.208:8080/\""
    const val BACKEND_ENDPOINT_PROD ="\"http://206.81.5.208:8080/\""

    const val TYPE_STRING = "String"
}