package com.urbanist.template.core.di.scope

import javax.inject.Scope

@Scope
@Retention
annotation class ActivityScope