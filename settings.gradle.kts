include(Modules.APP)

include(Modules.CORE)

include(Modules.FEATURE_SPLASH)
include(Modules.FEATURE_AUTH)
include(Modules.FEATURE_MAIN)
include(Modules.FEATURE_PROFILE_UNKNOWN)
include(Modules.FEATURE_PROFILE_KNOWN)
include(Modules.FEATURE_PROFILE)
include(Modules.FEATURE_VACANCIES)
include(Modules.FEATURE_SPECIFICATION)

include(Modules.MEMES_MVVM)
include(Modules.MEMES_BENDERADAPTER)
include(Modules.MEMES_UTILS)
include(Modules.MEMES_DATABINDING)
include(Modules.MEMES_NAVIGATION)