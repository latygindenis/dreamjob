package com.urbanist.template.navigation.vacancies.di

import com.urbanist.template.feature.vacancies.presentation.navigation.VacanciesNavCommandProvider
import com.urbanist.template.navigation.vacancies.VacanciesNavCommandProviderImpl
import dagger.Binds
import dagger.Module

@Module
interface VacanciesNavigationModule {

    @Binds
    fun bindVacanciesNavigator(impl: VacanciesNavCommandProviderImpl): VacanciesNavCommandProvider
}