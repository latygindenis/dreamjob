package com.urbanist.template.navigation.vacancydetails

import com.memebattle.memes.navigation.NavCommand
import com.urbanist.template.R
import com.urbanist.template.feature.details.presentation.navigation.VacancyDetailsNavCommandProvider
import javax.inject.Inject

class VacancyDetailsNavCommandProviderImpl @Inject constructor() :
    VacancyDetailsNavCommandProvider {

    override val toAuth: NavCommand = NavCommand(R.id.action_vacanciesListFragment_to_authFragment)

}