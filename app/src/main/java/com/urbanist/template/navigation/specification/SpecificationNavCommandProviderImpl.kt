package com.urbanist.template.navigation.specification

import com.memebattle.memes.navigation.NavCommand
import com.urbanist.template.R
import com.urbanist.template.feature.specificaton.presentation.navigation.SpecificationNavCommandProvider
import javax.inject.Inject

class SpecificationNavCommandProviderImpl @Inject constructor() : SpecificationNavCommandProvider {
    override val toVacancies: NavCommand =
        NavCommand(R.id.action_levelFragment_to_vacanciesListFragment)
    override val toLevel: NavCommand =
        NavCommand(R.id.action_specificationFragment_to_levelFragment)
}