package com.urbanist.template.navigation.specification.di

import com.urbanist.template.feature.specificaton.presentation.navigation.SpecificationNavCommandProvider
import com.urbanist.template.navigation.specification.SpecificationNavCommandProviderImpl
import dagger.Binds
import dagger.Module

@Module
interface SpecificationNavigationModule {

    @Binds
    fun bindSpecificationNavCommandProvide(impl: SpecificationNavCommandProviderImpl): SpecificationNavCommandProvider
}