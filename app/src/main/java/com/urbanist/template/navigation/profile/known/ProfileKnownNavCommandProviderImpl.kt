package com.urbanist.template.navigation.profile.known

import com.memebattle.memes.navigation.NavCommand
import com.urbanist.template.R
import com.urbanist.template.feature.known.presentation.navigation.ProfileKnownNavCommandProvider
import javax.inject.Inject

class ProfileKnownNavCommandProviderImpl @Inject constructor(

) : ProfileKnownNavCommandProvider {
    override val toSpec: NavCommand =
        NavCommand(R.id.action_mainFragment_to_specificationFragment)
}