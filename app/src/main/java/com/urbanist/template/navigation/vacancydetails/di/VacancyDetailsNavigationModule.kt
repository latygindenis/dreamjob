package com.urbanist.template.navigation.vacancydetails.di

import com.urbanist.template.feature.details.presentation.navigation.VacancyDetailsNavCommandProvider
import com.urbanist.template.navigation.vacancydetails.VacancyDetailsNavCommandProviderImpl
import dagger.Binds
import dagger.Module

@Module
interface VacancyDetailsNavigationModule {

    @Binds
    fun bindVacancyDetailsNavCommandProvider(impl: VacancyDetailsNavCommandProviderImpl): VacancyDetailsNavCommandProvider
}