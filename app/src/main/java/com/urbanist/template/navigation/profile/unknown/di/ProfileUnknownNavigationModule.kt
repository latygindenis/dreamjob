package com.urbanist.template.navigation.profile.unknown.di

import com.urbanist.template.feature.unknown.presentation.navigation.ProfileUnknownNavCommandProvider
import com.urbanist.template.navigation.profile.unknown.ProfileUnknownNavCommandProviderImpl
import dagger.Binds
import dagger.Module

@Module
interface ProfileUnknownNavigationModule {

    @Binds
    fun bindSettingsNavigator(impl: ProfileUnknownNavCommandProviderImpl): ProfileUnknownNavCommandProvider
}