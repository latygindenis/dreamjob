package com.urbanist.template.navigation.profile.known.di

import com.urbanist.template.feature.known.presentation.navigation.ProfileKnownNavCommandProvider
import com.urbanist.template.navigation.profile.known.ProfileKnownNavCommandProviderImpl
import dagger.Binds
import dagger.Module

@Module
interface ProfileNavigationModule {

    @Binds
    fun bindSettingsNavigator(impl: ProfileKnownNavCommandProviderImpl): ProfileKnownNavCommandProvider
}