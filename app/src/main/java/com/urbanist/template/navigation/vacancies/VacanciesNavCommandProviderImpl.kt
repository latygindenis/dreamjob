package com.urbanist.template.navigation.vacancies

import com.memebattle.memes.navigation.NavCommand
import com.urbanist.template.R
import com.urbanist.template.feature.vacancies.presentation.navigation.VacanciesNavCommandProvider
import javax.inject.Inject

class VacanciesNavCommandProviderImpl @Inject constructor() : VacanciesNavCommandProvider {

    override val toDetails: NavCommand =
        NavCommand(R.id.action_vacanciesListFragment_to_vacancyDetailsFragment)
}