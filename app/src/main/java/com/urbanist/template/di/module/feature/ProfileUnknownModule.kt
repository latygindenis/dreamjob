package com.urbanist.template.di.module.feature

import com.urbanist.template.navigation.profile.unknown.di.ProfileUnknownNavigationModule
import dagger.Module

@Module(includes = [ProfileUnknownNavigationModule::class])
interface ProfileUnknownModule