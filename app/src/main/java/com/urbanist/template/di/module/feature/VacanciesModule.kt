package com.urbanist.template.di.module.feature

import com.urbanist.template.navigation.vacancies.di.VacanciesNavigationModule
import dagger.Module

@Module(includes = [VacanciesNavigationModule::class])
interface VacanciesModule