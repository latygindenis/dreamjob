package com.urbanist.template.di.module.feature

import com.urbanist.template.navigation.profile.known.di.ProfileNavigationModule
import dagger.Module

@Module(includes = [ProfileNavigationModule::class])
interface ProfileKnownModule