package com.urbanist.template.di.module

import com.urbanist.template.core.di.scope.FragmentScope
import com.urbanist.template.feature.specificaton.presentation.SpecificationFragment
import com.urbanist.template.feature.specificaton.presentation.di.SpecificationFragmentModule
import com.urbanist.template.feature.vacancies.presentation.VacanciesListFragment
import com.urbanist.template.feature.auth.presentation.AuthFragment
import com.urbanist.template.feature.auth.presentation.di.AuthFragmentModule
import com.urbanist.template.feature.details.presentation.VacancyDetailsFragment
import com.urbanist.template.feature.details.presentation.di.VacanciesDetailsFragmentModule
import com.urbanist.template.feature.level.LevelFragment
import com.urbanist.template.feature.known.data.di.KnownSkillsDataModule
import com.urbanist.template.feature.unknown.data.di.UnknownSkillsDataModule
import com.urbanist.template.feature.known.presentation.ProfileKnownFragment
import com.urbanist.template.feature.splash.presentation.SplashFragment
import com.urbanist.template.feature.splash.presentation.di.SplashFragmentModule
import com.urbanist.template.feature.vacancies.data.di.VacanciesDataModule
import com.urbanist.template.feature.vacancies.presentation.di.VacanciesFragmentModule
import com.urbanist.template.feature.unknown.presentation.ProfileUnknownFragment
import com.urbanist.template.feature.unknown.presentation.di.ProfileUnknownFragmentModule
import com.urbanist.template.navigation.host.GlobalHostModule
import com.urbanist.template.navigation.profile.known.di.ProfileNavigationModule
import com.urbanist.template.navigation.specification.di.SpecificationNavigationModule
import com.urbanist.template.navigation.vacancies.di.VacanciesNavigationModule
import com.urbanist.template.navigation.vacancydetails.di.VacancyDetailsNavigationModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface AppActivityModule {

    @FragmentScope
    @ContributesAndroidInjector(
        modules = [
            SplashFragmentModule::class
        ]
    )
    fun splashFragmentInjector(): SplashFragment

    @FragmentScope
    @ContributesAndroidInjector(
        modules = [
            AuthFragmentModule::class
        ]
    )
    fun authFragmentInjector(): AuthFragment

    @FragmentScope
    @ContributesAndroidInjector(
        modules = [
            ProfileUnknownFragmentModule::class,
            UnknownSkillsDataModule::class
        ]
    )
    fun profileUnknownFragmentInjector(): ProfileUnknownFragment

    @FragmentScope
    @ContributesAndroidInjector(
        modules = [
            GlobalHostModule::class,
            ProfileNavigationModule::class,
            KnownSkillsDataModule::class
        ]
    )
    fun profileKnownFragmentInjector(): ProfileKnownFragment

    @FragmentScope
    @ContributesAndroidInjector(
        modules = [
            VacanciesFragmentModule::class,
            VacanciesNavigationModule::class,
            VacanciesDataModule::class
        ]
    )
    fun listFragmentInjector(): VacanciesListFragment

    @FragmentScope
    @ContributesAndroidInjector(
        modules = [
            SpecificationFragmentModule::class,
            SpecificationNavigationModule::class
        ]
    )
    fun specificationFragmentInjector(): SpecificationFragment

    @FragmentScope
    @ContributesAndroidInjector(
        modules = [
            SpecificationNavigationModule::class
        ]
    )
    fun levelFragmentInjector(): LevelFragment


    @FragmentScope
    @ContributesAndroidInjector(
        modules = [
            VacanciesDetailsFragmentModule::class,
            VacancyDetailsNavigationModule::class
        ]
    )
    fun bindsVacancyDetailsFragment(): VacancyDetailsFragment
}