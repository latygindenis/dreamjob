package com.urbanist.template.feature.user.data.datasource

import com.google.firebase.auth.FirebaseAuth
import javax.inject.Inject


class ProfileDataSourceImpl @Inject constructor(
    private val auth: FirebaseAuth
) : ProfileDataSource {

    override fun getEmail(): String? =
        auth.currentUser?.email
}
