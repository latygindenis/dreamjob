package com.urbanist.template.feature.user.data.datasource.di

import com.urbanist.template.feature.user.data.datasource.ProfileDataSource
import com.urbanist.template.feature.user.data.datasource.ProfileDataSourceImpl
import dagger.Binds
import dagger.Module

@Module
interface ProfileDataSourceModule {

    @Binds
    fun bindDataSource(impl: ProfileDataSourceImpl): ProfileDataSource
}