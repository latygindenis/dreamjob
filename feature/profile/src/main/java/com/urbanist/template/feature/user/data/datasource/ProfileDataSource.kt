package com.urbanist.template.feature.user.data.datasource

interface ProfileDataSource {

    fun getEmail(): String?
}