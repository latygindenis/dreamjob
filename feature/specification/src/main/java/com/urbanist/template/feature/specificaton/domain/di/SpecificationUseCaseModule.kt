package com.urbanist.template.feature.specificaton.domain.di

import com.urbanist.template.core.di.scope.FragmentScope
import com.urbanist.template.feature.specificaton.data.repository.di.SpecificationRepositoryModule
import com.urbanist.template.feature.specificaton.domain.signin.GetSpecificationInUseCase
import com.urbanist.template.feature.specificaton.domain.signin.GetSpecificationInUseCaseImpl
import dagger.Binds
import dagger.Module

@Module(includes = [SpecificationRepositoryModule::class])
interface SpecificationUseCaseModule {

	@Binds
	@FragmentScope
	fun bindSignInUseCase(impl: GetSpecificationInUseCaseImpl): GetSpecificationInUseCase
}