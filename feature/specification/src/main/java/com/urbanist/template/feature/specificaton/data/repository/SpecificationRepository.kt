package com.urbanist.template.feature.specificaton.data.repository

import com.urbanist.template.feature.specificaton.domain.entity.Specification
import io.reactivex.Single

interface SpecificationRepository {

    fun getSpecification(name: String): Single<Specification>
}