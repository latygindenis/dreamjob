package com.urbanist.template.feature.specificaton.data.datasource

import com.urbanist.template.feature.specificaton.data.datasource.model.SpecificationNetwork
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface SpecificationApi {

    @GET("/spec")
    fun getSpecification(
        @Query("name") name: String
    ): Single<SpecificationNetwork>
}