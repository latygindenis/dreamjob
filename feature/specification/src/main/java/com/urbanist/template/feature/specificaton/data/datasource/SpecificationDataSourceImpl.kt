package com.urbanist.template.feature.specificaton.data.datasource

import com.urbanist.template.feature.specificaton.data.datasource.model.SpecificationNetwork
import io.reactivex.Single
import javax.inject.Inject

class SpecificationDataSourceImpl @Inject constructor(
    private val api: SpecificationApi
) : SpecificationDataSource {

    override fun getSpecification(name: String): Single<SpecificationNetwork> =
        api.getSpecification(name)
}