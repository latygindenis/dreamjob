package com.urbanist.template.feature.specificaton.domain.signin

import com.urbanist.template.feature.specificaton.domain.entity.Specification
import io.reactivex.Single

interface GetSpecificationInUseCase {

    operator fun invoke(name: String): Single<Specification>
}