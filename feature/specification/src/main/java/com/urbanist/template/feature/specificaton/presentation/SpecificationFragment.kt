package com.urbanist.template.feature.specificaton.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import com.google.android.material.snackbar.Snackbar
import com.memebattle.memes.mvvm.fragment.BaseFragment
import com.memebattle.memes.navigation.navigate
import com.urbanist.template.feature.specificaton.R
import com.urbanist.template.feature.specificaton.databinding.FragmentSpecificationBinding
import com.urbanist.template.feature.specificaton.presentation.navigation.SpecificationNavCommandProvider
import kotlinx.android.synthetic.main.fragment_specification.*
import javax.inject.Inject

class SpecificationFragment : BaseFragment<FragmentSpecificationBinding>(),
    SpecificationViewModel.EventsListener {


    override val layoutId: Int = R.layout.fragment_specification
    private lateinit var autoSuggestAdapter: ArrayAdapter<String>

    @Inject
    lateinit var viewModel: SpecificationViewModel

    @Inject
    lateinit var navCommandProvider: SpecificationNavCommandProvider

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        viewModel.eventsDispatcher.bind(this, this)
        binding.viewModel = viewModel
        return view
    }

    override fun setSpecifications(list: ArrayList<String>) {
        specification.clearListSelection()
        autoSuggestAdapter = ArrayAdapter(
            activity,
            android.R.layout.simple_dropdown_item_1line, list
        )
        specification.setAdapter(autoSuggestAdapter)
        autoSuggestAdapter.notifyDataSetChanged()
        specification.setOnItemClickListener { _, _, position, _ ->
            specification.setText(autoSuggestAdapter.getItem(position))
        }
    }

    override fun showMessage(message: String) {
        view?.let { Snackbar.make(it, message, Snackbar.LENGTH_SHORT) }
    }

    override fun navigateLevel(specificationName: String) {
        navCommandProvider.toLevel.args = bundleOf("specificationName" to specificationName)
        navigate(navCommandProvider.toLevel)
    }
}
