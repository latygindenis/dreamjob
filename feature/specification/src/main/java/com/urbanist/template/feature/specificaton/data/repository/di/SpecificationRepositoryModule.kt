package com.urbanist.template.feature.specificaton.data.repository.di

import com.urbanist.template.core.di.scope.FragmentScope
import com.urbanist.template.feature.specificaton.data.datasource.di.SpecificationDataSourceModule
import com.urbanist.template.feature.specificaton.data.repository.SpecificationRepository
import com.urbanist.template.feature.specificaton.data.repository.SpecificationRepositoryImpl
import dagger.Binds
import dagger.Module

@Module(includes = [SpecificationDataSourceModule::class])
interface SpecificationRepositoryModule {

    @Binds
    @FragmentScope
    fun bindRepository(impl: SpecificationRepositoryImpl): SpecificationRepository
}