package com.urbanist.template.feature.specificaton.data.datasource.model

data class SpecificationNetwork(val spec: List<String>)