package com.urbanist.template.feature.specificaton.domain.entity

data class Specification(val nameList: List<String>)