package com.urbanist.template.feature.specificaton.data.datasource

import com.urbanist.template.feature.specificaton.data.datasource.model.SpecificationNetwork
import io.reactivex.Single

interface SpecificationDataSource {

    fun getSpecification(name: String): Single<SpecificationNetwork>

}