package com.urbanist.template.feature.specificaton.domain.signin

import com.urbanist.template.feature.specificaton.domain.entity.Specification
import com.urbanist.template.feature.specificaton.data.repository.SpecificationRepository
import io.reactivex.Single
import javax.inject.Inject

class GetSpecificationInUseCaseImpl @Inject constructor(
    private val specificationRepository: SpecificationRepository
) : GetSpecificationInUseCase {

    override operator fun invoke(name: String): Single<Specification> =
        specificationRepository.getSpecification(name)
}