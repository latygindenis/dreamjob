package com.urbanist.template.feature.level

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.memebattle.memes.mvvm.fragment.BaseFragment
import com.memebattle.memes.navigation.navigate
import com.urbanist.template.feature.specificaton.R
import com.urbanist.template.feature.specificaton.databinding.FragmentLevelBinding
import com.urbanist.template.feature.specificaton.presentation.navigation.SpecificationNavCommandProvider
import javax.inject.Inject

class LevelFragment : BaseFragment<FragmentLevelBinding>() {


    override val layoutId: Int = R.layout.fragment_level

    private var specificationName: String = ""
    private var level: String = "Middle"

    @Inject
    lateinit var navCommandProvider: SpecificationNavCommandProvider

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        specificationName = arguments?.getString("specificationName") ?: ""
        binding.nextButton.isEnabled = false
        binding.radioButton.setOnClickListener {
            level = "Intern"
            binding.nextButton.isEnabled = true
        }
        binding.radioButton2.setOnClickListener {
            level = "Junior"
            binding.nextButton.isEnabled = true
        }
        binding.radioButton3.setOnClickListener {
            level = "Middle"
            binding.nextButton.isEnabled = true
        }
        binding.radioButton4.setOnClickListener {
            level = "Senior"
            binding.nextButton.isEnabled = true
        }
        binding.nextButton.setOnClickListener {
            toVacansies()
        }
        return view
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        binding.nextButton.isEnabled = true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() {

    }

    private fun toVacansies() {
        navCommandProvider.toVacancies.args = bundleOf(
            "level" to level,
            "specification" to specificationName
        )
        navigate(navCommandProvider.toVacancies)
    }
}
