package com.urbanist.template.feature.specificaton.presentation

import android.content.Context
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import androidx.annotation.NonNull
import androidx.annotation.Nullable

class AutoSuggestAdapter(context: Context, resource: Int) : ArrayAdapter<String>(context, resource),
    Filterable{

    private var listData: ArrayList<String> = arrayListOf()

    fun setData(list: List<String>) {
        listData.clear()
        listData.addAll(list)
    }

    override fun getCount(): Int {
        return listData.size
    }

    @Nullable
    override fun getItem(position: Int): String? {
        return listData[position]
    }

    fun getObject(position: Int): String {
        return listData[position]
    }

    @NonNull
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                if (constraint != null) {
                    filterResults.values = listData
                    filterResults.count = listData.size
                }
                return filterResults
            }

            override fun publishResults(
                constraint: CharSequence,
                results: FilterResults?
            ) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }
        }
    }

}