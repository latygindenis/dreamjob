package com.urbanist.template.feature.specificaton.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcher
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcherOwner
import com.memebattle.memes.mvvm.viewmodel.BaseViewModel
import com.memebattle.memes.utils.ext.addTo
import com.urbanist.template.feature.specificaton.domain.signin.GetSpecificationInUseCase
import javax.inject.Inject

class SpecificationViewModel @Inject constructor(
    private val getSpecificationInUseCase: GetSpecificationInUseCase
) : BaseViewModel(), EventsDispatcherOwner<SpecificationViewModel.EventsListener> {

    override val eventsDispatcher: EventsDispatcher<EventsListener> = EventsDispatcher()

    val nameSpecification = MutableLiveData<String>()

    val buttonsEnable = MutableLiveData<Boolean>()

    val observer: Observer<String> = Observer {
        if (it.contains('\n')) {
            var res = ""
            for (i in it) {
                if (i != '\n') {
                    res += i
                }
            }
            nameSpecification.value = res
        }
        val name = nameSpecification.value ?: "specification"
        buttonsEnable.value = name.isNotEmpty()
        getSpecificationList()
    }

    init {
        buttonsEnable.value = false
        nameSpecification.observeForever(observer)
    }

    fun getSpecificationList() {
        val name = nameSpecification.value ?: ""
        name.let {
            getSpecificationInUseCase(it)
                .subscribe({
                    eventsDispatcher.dispatchEvent { setSpecifications(it.nameList as ArrayList<String>) }
                }, {
                    eventsDispatcher.dispatchEvent { showMessage(it.localizedMessage) }
                    buttonsEnable.value = true
                })
                .addTo(compositeDisposable)
        }
    }

    fun onChooseSpecification() {
        val name = nameSpecification.value ?: ""
        eventsDispatcher.dispatchEvent { navigateLevel(name) }
    }

    override fun onCleared() {
        super.onCleared()
        nameSpecification.removeObserver(observer)
    }

    interface EventsListener {
        fun showMessage(message: String)
        fun setSpecifications(list: ArrayList<String>)
        fun navigateLevel(specificationName: String)
    }
}