package com.urbanist.template.feature.specificaton.presentation.navigation

import com.memebattle.memes.navigation.NavCommand

interface SpecificationNavCommandProvider {

    val toVacancies: NavCommand
    val toLevel: NavCommand
}