package com.urbanist.template.feature.specificaton.data.datasource.di

import com.urbanist.template.core.di.scope.FragmentScope
import com.urbanist.template.feature.specificaton.data.datasource.SpecificationDataSource
import com.urbanist.template.feature.specificaton.data.datasource.SpecificationDataSourceImpl
import com.urbanist.template.feature.specificaton.data.datasource.SpecificationApi
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class SpecificationDataSourceModule {


    @Provides
    @FragmentScope
    fun provideSpecificationApi(retrofit: Retrofit): SpecificationApi =
        retrofit.create(SpecificationApi::class.java)

    @Provides
    @FragmentScope
    fun provideSpecificationDataSource(api: SpecificationApi): SpecificationDataSource =
        SpecificationDataSourceImpl(api)
}