package com.urbanist.template.feature.specificaton.data.repository

import com.urbanist.template.feature.specificaton.data.datasource.SpecificationDataSource
import com.urbanist.template.feature.specificaton.domain.entity.Specification
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class SpecificationRepositoryImpl @Inject constructor(
    private val specificationDataSource: SpecificationDataSource
) : SpecificationRepository {
    override fun getSpecification(name: String): Single<Specification> =
        specificationDataSource.getSpecification(name)
            .map { Specification(it.spec) }
            .observeOn(AndroidSchedulers.mainThread())


}