package com.urbanist.template.feature.specificaton.presentation.di

import androidx.lifecycle.ViewModelProvider
import com.memebattle.memes.mvvm.viewmodel.ViewModelFactory
import com.urbanist.template.core.di.scope.FragmentScope
import com.urbanist.template.feature.specificaton.domain.signin.GetSpecificationInUseCase
import com.urbanist.template.feature.specificaton.domain.di.SpecificationUseCaseModule
import com.urbanist.template.feature.specificaton.presentation.SpecificationFragment
import com.urbanist.template.feature.specificaton.presentation.SpecificationViewModel
import dagger.Module
import dagger.Provides

@Module(includes = [SpecificationUseCaseModule::class])
class SpecificationFragmentModule {

    @Provides
    @FragmentScope
    fun provideViewModel(
        owner: SpecificationFragment,
        getSpecificationInUseCase: GetSpecificationInUseCase
    ): SpecificationViewModel = ViewModelFactory {
        SpecificationViewModel(
            getSpecificationInUseCase
        )
    }.let { viewModelFactory ->
        ViewModelProvider(owner, viewModelFactory)[SpecificationViewModel::class.java]
    }
}