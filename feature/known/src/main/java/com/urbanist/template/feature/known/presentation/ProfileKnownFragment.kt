package com.urbanist.template.feature.known.presentation

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.memebattle.memes.benderadapter.BinderAdapter
import com.memebattle.memes.mvvm.fragment.BaseFragment
import com.memebattle.memes.navigation.NavHost
import com.memebattle.memes.navigation.navigate
import com.urbanist.template.feature.known.R
import com.urbanist.template.feature.known.databinding.FragmentProfileKnownBinding
import com.urbanist.template.feature.known.presentation.navigation.ProfileKnownNavCommandProvider
import javax.inject.Inject

class ProfileKnownFragment : BaseFragment<FragmentProfileKnownBinding>(),
	ProfileKnownViewModel.EventsListener {

	override val layoutId: Int = R.layout.fragment_profile_known

	@Inject
	lateinit var knownNavCommandProvider: ProfileKnownNavCommandProvider

	@Inject
	lateinit var knownViewModel: ProfileKnownViewModel

	private val adapter = BinderAdapter()

	@Inject
	lateinit var commandProvider: ProfileKnownNavCommandProvider

	@Inject
	lateinit var host: NavHost

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		val view = super.onCreateView(inflater, container, savedInstanceState)
		knownViewModel.eventsDispatcher.bind(this, this)
		binding.viewModel = knownViewModel
		binding.skillsList.adapter = adapter

		val preferences = requireActivity().getSharedPreferences("SELECTED_VACACIES_PREF", Context.MODE_PRIVATE)!!

		val vacancyName = preferences.getString("vacancyName", "")
		val city = preferences.getString("city", "")
		val companyName = preferences.getString("companyName", "")
		val salary = preferences.getString("salary", "")

		binding.vacancyName.text = vacancyName
		binding.vacancyCity.text = city
		binding.vacancyCompany.text = companyName
		binding.vacancySalary.text = salary

		return view
	}

	override fun chooseSpec() {
		navigate(commandProvider.toSpec, Navigation.findNavController(requireActivity(), host.id))
	}
}
