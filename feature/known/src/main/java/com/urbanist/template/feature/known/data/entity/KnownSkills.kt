package com.urbanist.template.feature.known.data.entity

data class KnownSkills(val known: List<String>)