package com.urbanist.template.feature.known.presentation.navigation

import com.memebattle.memes.navigation.NavCommand

interface ProfileKnownNavCommandProvider {
    val toSpec: NavCommand
}