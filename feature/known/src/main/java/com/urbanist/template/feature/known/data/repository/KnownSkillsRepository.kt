package com.urbanist.template.feature.known.data.repository

import com.urbanist.template.feature.known.data.Score
import io.reactivex.Single

interface KnownSkillsRepository {

    fun getKnownSkills(): Single<List<String>>

    fun getScore(): Single<Score>
}