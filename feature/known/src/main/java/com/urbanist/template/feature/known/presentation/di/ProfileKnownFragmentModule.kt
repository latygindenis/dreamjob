package com.urbanist.template.feature.known.presentation.di

import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.memebattle.memes.mvvm.viewmodel.ViewModelFactory
import com.urbanist.template.core.di.scope.FragmentScope
import com.urbanist.template.feature.known.domain.GetKnownSkillsUseCase
import com.urbanist.template.feature.known.domain.ScoreUseCase
import com.urbanist.template.feature.known.presentation.ProfileKnownFragment
import com.urbanist.template.feature.known.presentation.ProfileKnownViewModel
import dagger.Module
import dagger.Provides

@Module
class ProfileKnownFragmentModule {

    @Provides
    @FragmentScope
    fun provideViewModel(
        owner: ProfileKnownFragment,
        getKnownSkillsUseCase: GetKnownSkillsUseCase,
        scoreUseCase: ScoreUseCase,
        auth: FirebaseAuth
    ): ProfileKnownViewModel = ViewModelFactory {
        ProfileKnownViewModel(
            getKnownSkillsUseCase,
            scoreUseCase,
            auth
        )
    }.let { viewModelFactory ->
        ViewModelProvider(owner, viewModelFactory)[ProfileKnownViewModel::class.java]
    }
}