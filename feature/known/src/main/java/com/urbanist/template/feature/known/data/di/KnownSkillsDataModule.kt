package com.urbanist.template.feature.known.data.di

import com.urbanist.template.feature.known.data.datasource.KnownSkillsDataSource
import com.urbanist.template.feature.known.data.datasource.KnownSkillsApiDataSorceImpl
import com.urbanist.template.feature.known.data.api.di.KnownSkillsApiModule
import com.urbanist.template.feature.known.data.repository.KnownSkillsRepository
import com.urbanist.template.feature.known.data.repository.KnownSkillsRepositoryImpl
import com.urbanist.template.feature.user.data.datasource.di.ProfileDataSourceModule
import dagger.Binds
import dagger.Module

@Module(includes = [KnownSkillsApiModule::class, ProfileDataSourceModule::class])
interface KnownSkillsDataModule {

    @Binds
    fun bindRepository(impl: KnownSkillsRepositoryImpl): KnownSkillsRepository

    @Binds
    fun bindDataSorce(impl: KnownSkillsApiDataSorceImpl): KnownSkillsDataSource
}