package com.urbanist.template.feature.known.data.api.di

import com.urbanist.template.feature.known.data.api.KnownSkillsApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class KnownSkillsApiModule {

    @Provides
    fun provideApi(retrofit: Retrofit): KnownSkillsApi =
        retrofit.create(KnownSkillsApi::class.java)
}