package com.urbanist.template.feature.known.data

data class Score(val score: Float)