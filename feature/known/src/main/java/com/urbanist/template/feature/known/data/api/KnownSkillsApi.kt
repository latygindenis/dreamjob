package com.urbanist.template.feature.known.data.api

import com.urbanist.template.feature.known.data.Score
import com.urbanist.template.feature.known.data.entity.KnownSkills
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface KnownSkillsApi {

    @GET("/profile/known")
    fun getKnownSkills(@Query("email") email: String): Single<KnownSkills>

    @GET("/profile/score")
    fun getScore(@Query("email") email: String): Single<Score>
}