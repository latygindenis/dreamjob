package com.urbanist.template.feature.known.presentation

import androidx.databinding.ViewDataBinding
import com.memebattle.memes.benderadapter.BindingClass
import com.urbanist.template.feature.known.R

class KnownSkillUiModel (
    val name: String
) : BindingClass {
    override val layoutId: Int = R.layout.item_known_skill

    override fun bind(viewDataBinding: ViewDataBinding) {
        viewDataBinding.setVariable(com.urbanist.template.feature.known.BR.uiModel, this)
    }

}