package com.urbanist.template.feature.known.domain

import com.urbanist.template.feature.known.data.repository.KnownSkillsRepository
import io.reactivex.Single
import javax.inject.Inject

class GetKnownSkillsUseCase @Inject constructor(
    private val knownSkillsRepository: KnownSkillsRepository
) {

    operator fun invoke(): Single<List<String>> =
        knownSkillsRepository.getKnownSkills()
}