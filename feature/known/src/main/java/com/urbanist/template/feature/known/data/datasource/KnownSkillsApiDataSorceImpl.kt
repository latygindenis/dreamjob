package com.urbanist.template.feature.known.data.datasource

import com.urbanist.template.feature.known.data.Score
import com.urbanist.template.feature.known.data.api.KnownSkillsApi
import com.urbanist.template.feature.known.data.entity.KnownSkills
import io.reactivex.Single
import javax.inject.Inject

class KnownSkillsApiDataSorceImpl @Inject constructor(
    private val knownSkillsApi: KnownSkillsApi
) : KnownSkillsDataSource {

    override fun getKnownSkills(email: String): Single<KnownSkills> =
        knownSkillsApi.getKnownSkills(email)

    override fun getScore(email: String): Single<Score> =
        knownSkillsApi.getScore(email)
}