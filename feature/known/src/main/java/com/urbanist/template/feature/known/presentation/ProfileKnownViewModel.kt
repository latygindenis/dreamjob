package com.urbanist.template.feature.known.presentation

import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcher
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcherOwner
import com.memebattle.memes.mvvm.viewmodel.BaseViewModel
import com.memebattle.memes.utils.ext.addTo
import com.urbanist.template.feature.known.domain.GetKnownSkillsUseCase
import com.urbanist.template.feature.known.domain.ScoreUseCase
import javax.inject.Inject

class ProfileKnownViewModel @Inject constructor(
	knownSkillsUseCase: GetKnownSkillsUseCase,
	scoreUseCase: ScoreUseCase,
	val auth: FirebaseAuth
) : BaseViewModel(), EventsDispatcherOwner<ProfileKnownViewModel.EventsListener> {

	override val eventsDispatcher: EventsDispatcher<EventsListener> = EventsDispatcher()

	val skills = MutableLiveData<List<KnownSkillUiModel>>()

	val score: MutableLiveData<String> = MutableLiveData()

	init {
		knownSkillsUseCase()
			.subscribe({ it ->
				skills.value = it.map { KnownSkillUiModel(it) }
			}, { t ->
				t
			})
			.addTo(compositeDisposable)

		scoreUseCase()
			.subscribe({
				score.value = "${it.score*100}%"
			}, { t ->
				t
			})
			.addTo(compositeDisposable)
	}

	fun onChooseVacancyClick() {
		auth.signOut()
		eventsDispatcher.dispatchEvent { chooseSpec() }
	}

	interface EventsListener {
		fun chooseSpec()
	}
}