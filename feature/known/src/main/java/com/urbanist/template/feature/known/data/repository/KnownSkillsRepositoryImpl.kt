package com.urbanist.template.feature.known.data.repository

import com.urbanist.template.feature.known.data.Score
import com.urbanist.template.feature.known.data.datasource.KnownSkillsDataSource
import com.urbanist.template.feature.user.data.datasource.ProfileDataSource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class KnownSkillsRepositoryImpl @Inject constructor(
    private val knownSkillsDataSource: KnownSkillsDataSource,
    private val profileDataSource: ProfileDataSource
) : KnownSkillsRepository {
    override fun getKnownSkills(): Single<List<String>> =

        knownSkillsDataSource.getKnownSkills(profileDataSource.getEmail()!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { knownSkills ->
                knownSkills.known
            }

    override fun getScore(): Single<Score> =
        knownSkillsDataSource.getScore(profileDataSource.getEmail()!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

}