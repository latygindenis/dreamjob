package com.urbanist.template.feature.known.domain

import com.urbanist.template.feature.known.data.repository.KnownSkillsRepository
import javax.inject.Inject

class ScoreUseCase @Inject constructor(
    private val knownSkillsRepository: KnownSkillsRepository
) {
    operator fun invoke() =
        knownSkillsRepository.getScore()
}