package com.urbanist.template.feature.known.data.datasource

import com.urbanist.template.feature.known.data.Score
import com.urbanist.template.feature.known.data.entity.KnownSkills
import io.reactivex.Single

interface KnownSkillsDataSource {

    fun getKnownSkills(email: String): Single<KnownSkills>
    fun getScore(email: String): Single<Score>
}