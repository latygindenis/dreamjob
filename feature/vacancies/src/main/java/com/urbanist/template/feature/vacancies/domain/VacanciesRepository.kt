package com.urbanist.template.feature.vacancies.domain

import com.urbanist.template.feature.vacancies.domain.model.Vacancy
import io.reactivex.Single

interface VacanciesRepository {

    fun getVacancies(name: String, level: String) : Single<List<Vacancy>>
}