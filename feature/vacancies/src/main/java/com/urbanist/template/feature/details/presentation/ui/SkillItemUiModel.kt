package com.urbanist.template.feature.details.presentation.ui

import androidx.databinding.ObservableBoolean
import androidx.databinding.ViewDataBinding
import com.memebattle.memes.benderadapter.BindingClass
import com.urbanist.template.feature.vacancies.R
import java.io.Serializable

data class SkillItemUiModel(
    val name: String,
    val selected: ObservableBoolean = ObservableBoolean(false)
) : BindingClass, Serializable {
    override val layoutId: Int = R.layout.item_skill

    override fun bind(viewDataBinding: ViewDataBinding) {
        viewDataBinding.setVariable(com.urbanist.template.feature.vacancies.BR.uiModel, this)
    }
}