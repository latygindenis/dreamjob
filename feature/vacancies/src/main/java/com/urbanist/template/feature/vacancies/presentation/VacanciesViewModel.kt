package com.urbanist.template.feature.vacancies.presentation

import android.os.Bundle
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcher
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcherOwner
import com.memebattle.memes.mvvm.viewmodel.BaseViewModel
import com.memebattle.memes.utils.ext.addTo
import com.urbanist.template.feature.vacancies.domain.GetVacanciesUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class VacanciesViewModel @Inject constructor(
    private val getVacanciesUseCase: GetVacanciesUseCase
) : BaseViewModel(), EventsDispatcherOwner<VacanciesViewModel.EventsListener> {

    val vacancies = MutableLiveData<List<VacancyUiModel>>()
    val emptyTextVisibility = MutableLiveData<Int>()
    override val eventsDispatcher: EventsDispatcher<EventsListener> = EventsDispatcher()

    init {
        emptyTextVisibility.value = View.GONE
    }

    fun onBindArgument(bundle: Bundle?) {
        val level = bundle?.getString("level")!!
        val specification = bundle?.getString("specification")!!

        getVacanciesUseCase(name = specification, level = level)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.isEmpty()) {
                    emptyTextVisibility.value = View.VISIBLE
                } else {
                    it.size
                    vacancies.value = it.map {
                        VacancyUiModel(
                            vacancyName = it.name,
                            city = it.city,
                            salary = getSalaryString(it.salary),
                            companyName = it.companyName,
                            skills = it.skills
                        )
                    }
                }
            }, {
                it.localizedMessage
            })
            .addTo(compositeDisposable)
    }

    private fun getSalaryString(salary: Int): String =
        if (salary == 0) {
            "Не указано"
        } else {
            "$salary RUB"
        }

    interface EventsListener
}