package com.urbanist.template.feature.details.presentation.navigation

import com.memebattle.memes.navigation.NavCommand

interface VacancyDetailsNavCommandProvider {

    val toAuth : NavCommand
}