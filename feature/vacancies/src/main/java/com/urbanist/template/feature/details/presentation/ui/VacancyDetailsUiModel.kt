package com.urbanist.template.feature.details.presentation.ui

import androidx.databinding.ObservableField

data class VacancyDetailsUiModel(
    val vacancyName: ObservableField<String> =  ObservableField(""),
    val city: ObservableField<String> =  ObservableField(""),
    val companyName: ObservableField<String> =  ObservableField("") ,
    val salary: ObservableField<String> =  ObservableField("")
)