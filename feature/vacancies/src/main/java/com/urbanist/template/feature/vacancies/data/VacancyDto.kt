package com.urbanist.template.feature.vacancies.data

import com.google.gson.annotations.SerializedName


data class VacancyDto(
    val city: String,
    @SerializedName("company_name") val companyName: String,
    val level: String,
    val name: String,
    val salary: Int,
    val skills: List<String>
)