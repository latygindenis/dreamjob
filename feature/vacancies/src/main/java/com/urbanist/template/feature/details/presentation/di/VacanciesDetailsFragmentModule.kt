package com.urbanist.template.feature.details.presentation.di

import androidx.lifecycle.ViewModelProvider
import com.memebattle.memes.mvvm.viewmodel.ViewModelFactory
import com.urbanist.template.core.di.scope.FragmentScope
import com.urbanist.template.feature.details.presentation.VacancyDetailsFragment
import com.urbanist.template.feature.details.presentation.VacancyDetailsViewModel
import dagger.Module
import dagger.Provides

@Module
class VacanciesDetailsFragmentModule {

    @Provides
    @FragmentScope
    fun provideViewModel(
        owner: VacancyDetailsFragment
    ): VacancyDetailsViewModel = ViewModelFactory {
        VacancyDetailsViewModel()
    }.let { viewModelFactory ->
        ViewModelProvider(owner, viewModelFactory)[VacancyDetailsViewModel::class.java]
    }
}