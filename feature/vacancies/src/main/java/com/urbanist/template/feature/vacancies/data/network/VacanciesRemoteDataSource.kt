package com.urbanist.template.feature.vacancies.data.network

import com.urbanist.template.feature.vacancies.data.VacancyDto
import io.reactivex.Single
import javax.inject.Inject

interface VacanciesRemoteDataSource {

    fun getVacancies(name: String, level: String): Single<List<VacancyDto>>
}

class VacanciesRemoteDataSourceImpl @Inject constructor(
    private val vacanciesApi: VacanciesApi
) :
    VacanciesRemoteDataSource {

    override fun getVacancies(name: String, level: String): Single<List<VacancyDto>> =
        vacanciesApi.getVacancies(name, level)

}