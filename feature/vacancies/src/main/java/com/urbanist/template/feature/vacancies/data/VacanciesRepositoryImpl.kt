package com.urbanist.template.feature.vacancies.data

import com.urbanist.template.feature.vacancies.data.network.VacanciesRemoteDataSource
import com.urbanist.template.feature.vacancies.domain.VacanciesRepository
import com.urbanist.template.feature.vacancies.domain.model.Vacancy
import io.reactivex.Single
import javax.inject.Inject

class VacanciesRepositoryImpl @Inject constructor(
    private val vacanciesRemoteDataSource: VacanciesRemoteDataSource,
    private val vacancyConverter: VacancyConverter
) : VacanciesRepository {

    override fun getVacancies(name: String, level: String): Single<List<Vacancy>> =
        vacanciesRemoteDataSource.getVacancies(name, level)
            .map { it.map(vacancyConverter::convert)}
}