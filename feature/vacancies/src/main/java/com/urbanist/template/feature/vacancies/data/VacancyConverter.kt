package com.urbanist.template.feature.vacancies.data

import com.urbanist.template.feature.vacancies.domain.model.Vacancy
import javax.inject.Inject

class VacancyConverter @Inject constructor() {

    fun convert(vacacyDto: VacancyDto): Vacancy =
        with(vacacyDto) {
            Vacancy(
                level = level,
                name = name,
                salary = salary,
                companyName = companyName,
                city = city,
                skills = skills
            )
        }
}