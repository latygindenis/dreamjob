package com.urbanist.template.feature.vacancies.presentation.di

import androidx.lifecycle.ViewModelProvider
import com.memebattle.memes.mvvm.viewmodel.ViewModelFactory
import com.urbanist.template.core.di.scope.FragmentScope
import com.urbanist.template.feature.vacancies.domain.GetVacanciesUseCase
import com.urbanist.template.feature.vacancies.presentation.VacanciesListFragment
import com.urbanist.template.feature.vacancies.presentation.VacanciesViewModel
import dagger.Module
import dagger.Provides

@Module
class VacanciesFragmentModule {

	@Provides
	@FragmentScope
	fun provideViewModel(
		owner: VacanciesListFragment,
		getVacanciesUseCase: GetVacanciesUseCase
	): VacanciesViewModel = ViewModelFactory {
        VacanciesViewModel(
			getVacanciesUseCase
        )
	}.let { viewModelFactory ->
		ViewModelProvider(owner, viewModelFactory)[VacanciesViewModel::class.java]
	}
}