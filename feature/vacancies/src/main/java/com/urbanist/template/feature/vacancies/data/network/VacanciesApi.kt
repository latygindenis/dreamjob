package com.urbanist.template.feature.vacancies.data.network

import com.urbanist.template.feature.vacancies.data.VacancyDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface VacanciesApi {

    @GET("/vacancies")
    fun getVacancies(
        @Query("name") name: String,
        @Query("lvl") level: String
    ): Single<List<VacancyDto>>
}