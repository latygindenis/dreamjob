package com.urbanist.template.feature.details.presentation

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcher
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcherOwner
import com.memebattle.memes.mvvm.viewmodel.BaseViewModel
import com.urbanist.template.feature.details.presentation.ui.SkillItemUiModel
import com.urbanist.template.feature.details.presentation.ui.VacancyDetailsUiModel
import com.urbanist.template.feature.vacancies.presentation.VacancyUiModel
import javax.inject.Inject

class VacancyDetailsViewModel @Inject constructor() : BaseViewModel(),
    EventsDispatcherOwner<VacancyDetailsViewModel.EventsListener> {


    override val eventsDispatcher: EventsDispatcher<EventsListener> = EventsDispatcher()

    val skills = MutableLiveData<List<SkillItemUiModel>>()

    val vacancyDetailsUiModel = VacancyDetailsUiModel()

    fun onBindArgument(arguments: Bundle) {
        val vacancyUiModel = arguments.get("vacancyUiModel") as VacancyUiModel

        skills.value = vacancyUiModel.skills.map {
            SkillItemUiModel(
                name = it
            )
        }

        vacancyDetailsUiModel.vacancyName.set(vacancyUiModel.vacancyName)
        vacancyDetailsUiModel.city.set(vacancyUiModel.city)
        vacancyDetailsUiModel.companyName.set(vacancyUiModel.companyName)
        vacancyDetailsUiModel.salary.set(vacancyUiModel.salary)
    }

    interface EventsListener
}