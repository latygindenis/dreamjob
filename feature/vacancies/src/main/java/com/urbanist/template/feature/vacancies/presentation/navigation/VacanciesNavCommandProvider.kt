package com.urbanist.template.feature.vacancies.presentation.navigation

import com.memebattle.memes.navigation.NavCommand

interface VacanciesNavCommandProvider {

    val toDetails : NavCommand
}