package com.urbanist.template.feature.vacancies.domain.model

data class Vacancy(
    val level: String,
    val name: String,
    val salary: Int,
    val companyName: String,
    val city: String,
    val skills: List<String>
)