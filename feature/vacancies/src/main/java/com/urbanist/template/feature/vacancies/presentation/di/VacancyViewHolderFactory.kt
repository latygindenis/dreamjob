package com.urbanist.template.feature.vacancies.presentation.di

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.memebattle.memes.benderadapter.BindingViewHolder
import com.memebattle.memes.benderadapter.ViewHolderFactory
import com.urbanist.template.feature.vacancies.R
import com.urbanist.template.feature.vacancies.databinding.ItemVacancyBinding
import com.urbanist.template.feature.vacancies.presentation.VacancyUiModel

class VacancyViewHolderFactory(
    private val onItemClick: (uiModel: VacancyUiModel) -> Unit
) : ViewHolderFactory {

    override fun create(parent: ViewGroup): BindingViewHolder<ViewDataBinding> =
        BindingViewHolder<ItemVacancyBinding>(
            parent,
            R.layout.item_vacancy
        ).apply {
            binding.itemVacancyCardView.setOnClickListener {
                onItemClick(binding.uiModel!!)
            }
        }
}