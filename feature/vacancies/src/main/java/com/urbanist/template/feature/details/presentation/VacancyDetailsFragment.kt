package com.urbanist.template.feature.details.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.edit
import androidx.core.os.bundleOf
import com.memebattle.memes.benderadapter.BinderAdapter
import com.memebattle.memes.benderadapter.BindingClass
import com.memebattle.memes.mvvm.fragment.BaseFragment
import com.memebattle.memes.navigation.navigate
import com.urbanist.template.feature.details.presentation.navigation.VacancyDetailsNavCommandProvider
import com.urbanist.template.feature.details.presentation.ui.SkillItemUiModel
import com.urbanist.template.feature.vacancies.R
import com.urbanist.template.feature.vacancies.databinding.FragmentVacancyDetailsBinding
import javax.inject.Inject

const val SELECTED_VACACIES_PREF = "SELECTED_VACACIES_PREF"

class VacancyDetailsFragment : BaseFragment<FragmentVacancyDetailsBinding>(),
    VacancyDetailsViewModel.EventsListener {

    @Inject
    lateinit var viewModel: VacancyDetailsViewModel

    @Inject
    lateinit var navCommandProvider: VacancyDetailsNavCommandProvider

    override val layoutId: Int = R.layout.fragment_vacancy_details

    private val adapter = BinderAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        viewModel.eventsDispatcher.bind(this, this)
        viewModel.onBindArgument(arguments!!)
        binding.viewModel = viewModel

        binding.saveButton.setOnClickListener {
            activity?.getSharedPreferences(SELECTED_VACACIES_PREF, Context.MODE_PRIVATE)!!
                .edit {
                    putString("vacancyName", viewModel.vacancyDetailsUiModel.vacancyName.get())
                    putString("city", viewModel.vacancyDetailsUiModel.city.get())
                    putString("companyName", viewModel.vacancyDetailsUiModel.companyName.get())
                    putString("salary", viewModel.vacancyDetailsUiModel.salary.get())
                }
            val skillsList = getSelectedSkills(adapter.itemList)
            navCommandProvider.toAuth.args= bundleOf("skillsList" to  skillsList)
            navigate(navCommandProvider.toAuth)
        }

        binding.titleUiModel = viewModel.vacancyDetailsUiModel
        binding.skillList.adapter = adapter
        return view
    }

    private fun getSelectedSkills(itemList: List<BindingClass>): List<SkillItemUiModel> =
        itemList.map { it as SkillItemUiModel }
}