package com.urbanist.template.feature.vacancies.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.memebattle.memes.benderadapter.BinderAdapter
import com.memebattle.memes.mvvm.fragment.BaseFragment
import com.memebattle.memes.navigation.navigate
import com.memebattle.memes.utils.info.toast
import com.urbanist.template.feature.vacancies.R
import com.urbanist.template.feature.vacancies.databinding.FragmentVacanciesListBinding
import com.urbanist.template.feature.vacancies.presentation.di.VacancyViewHolderFactory
import com.urbanist.template.feature.vacancies.presentation.navigation.VacanciesNavCommandProvider
import javax.inject.Inject

class VacanciesListFragment : BaseFragment<FragmentVacanciesListBinding>(),
    VacanciesViewModel.EventsListener {

    override val layoutId: Int = R.layout.fragment_vacancies_list

    @Inject
    lateinit var navCommandProvider: VacanciesNavCommandProvider

    @Inject
    lateinit var viewModel: VacanciesViewModel

    private val onItemClick: (uiModel: VacancyUiModel) -> Unit = { uiModel ->

        navCommandProvider.toDetails.args = bundleOf(
            "vacancyUiModel" to uiModel
        )
        navigate(navCommandProvider.toDetails)
    }

    private val vacancyViewHolderFactory = VacancyViewHolderFactory(onItemClick)

    private val adapter = BinderAdapter(
        mapOf(R.layout.item_vacancy to vacancyViewHolderFactory)
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        viewModel.eventsDispatcher.bind(this, this)
        viewModel.onBindArgument(arguments!!)
        binding.viewModel = viewModel
        binding.vacanciesList.adapter = adapter
        return view
    }
}
