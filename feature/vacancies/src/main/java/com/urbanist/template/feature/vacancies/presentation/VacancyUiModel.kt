package com.urbanist.template.feature.vacancies.presentation

import androidx.databinding.ViewDataBinding
import com.memebattle.memes.benderadapter.BindingClass
import com.urbanist.template.feature.vacancies.R
import java.io.Serializable

data class VacancyUiModel(
    val vacancyName: String,
    val city: String,
    val companyName: String,
    val salary: String,
    val skills: List<String>
) : BindingClass, Serializable {

    override val layoutId: Int = R.layout.item_vacancy

    override fun bind(viewDataBinding: ViewDataBinding) {
        viewDataBinding.setVariable(com.urbanist.template.feature.vacancies.BR.uiModel, this)
    }
}