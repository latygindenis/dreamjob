package com.urbanist.template.feature.vacancies.data.di

import com.urbanist.template.core.di.scope.ApplicationScope
import com.urbanist.template.feature.vacancies.data.VacanciesRepositoryImpl
import com.urbanist.template.feature.vacancies.data.network.VacanciesApi
import com.urbanist.template.feature.vacancies.data.network.VacanciesRemoteDataSource
import com.urbanist.template.feature.vacancies.data.network.VacanciesRemoteDataSourceImpl
import com.urbanist.template.feature.vacancies.domain.VacanciesRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit

@Module
abstract class VacanciesDataModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideVacanciesApi(retrofit: Retrofit): VacanciesApi =
            retrofit.create(VacanciesApi::class.java)
    }

    @Binds
    @Reusable
    abstract fun bindVacanciesDataSource(impl: VacanciesRemoteDataSourceImpl): VacanciesRemoteDataSource

    @Binds
    @Reusable
    abstract fun bindVacanciesRepository(impl: VacanciesRepositoryImpl): VacanciesRepository
}