package com.urbanist.template.feature.vacancies.domain

import com.urbanist.template.feature.vacancies.domain.model.Vacancy
import io.reactivex.Single
import javax.inject.Inject

class GetVacanciesUseCase @Inject constructor(
    private val vacanciesRepository: VacanciesRepository
) {

    operator fun invoke(name: String, level: String): Single<List<Vacancy>> =
        vacanciesRepository.getVacancies(name, level)
}