package com.urbanist.template.feature.auth.presentation

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcher
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcherOwner
import com.memebattle.memes.mvvm.viewmodel.BaseViewModel
import com.memebattle.memes.utils.ext.addTo
import com.urbanist.template.feature.auth.domain.signin.SignInUseCase
import com.urbanist.template.feature.auth.domain.signup.SignUpUseCase
import com.urbanist.template.feature.details.presentation.ui.SkillItemUiModel
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class AuthViewModel @Inject constructor(
    private val signInUseCase: SignInUseCase,
    private val signUpUseCase: SignUpUseCase
) : BaseViewModel(), EventsDispatcherOwner<AuthViewModel.EventsListener> {

    override val eventsDispatcher: EventsDispatcher<EventsListener> = EventsDispatcher()

    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    val buttonsEnable = MutableLiveData<Boolean>()

    init {
        buttonsEnable.value = true
    }

    private lateinit var skillList: List<SkillItemUiModel>

    fun onBindArguments(bundle: Bundle) {
       skillList = bundle.get("skillsList") as List<SkillItemUiModel>
    }

    fun onSignInClick() {
        val email = email.value
        val password = password.value
        if (email == null || email == "") {
            eventsDispatcher.dispatchEvent { showMessage("Заполните поле почты") }
            return
        }
        if (password == null || password == "") {
            eventsDispatcher.dispatchEvent { showMessage("Заполните поле пароля") }
            return
        }
        buttonsEnable.value = false
        signInUseCase(email, password, skillList)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                eventsDispatcher.dispatchEvent { navigateMain() }
            }, {
                eventsDispatcher.dispatchEvent { showMessage(it.localizedMessage) }
                buttonsEnable.value = true
            })
            .addTo(compositeDisposable)
    }

    fun onSignUpClick() {
        val email = email.value
        val password = password.value
        if (email == null || email == "") {
            eventsDispatcher.dispatchEvent { showMessage("Заполните поле почты") }
            return
        }
        if (password == null || password == "") {
            eventsDispatcher.dispatchEvent { showMessage("Заполните поле пароля") }
            return
        }
        buttonsEnable.value = false
        signUpUseCase(email, password, skillList)
            .subscribe({
                eventsDispatcher.dispatchEvent { navigateMain() }
            }, {
                eventsDispatcher.dispatchEvent { showMessage(it.localizedMessage) }
                buttonsEnable.value = true
            })
            .addTo(compositeDisposable)
    }

    interface EventsListener {
        fun showMessage(message: String)
        fun navigateMain()
    }
}