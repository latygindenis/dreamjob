package com.urbanist.template.feature.auth.data.repository

import com.urbanist.template.feature.auth.domain.entity.AuthUser
import com.urbanist.template.feature.details.presentation.ui.SkillItemUiModel
import io.reactivex.Single

interface AuthRepository {

    fun signIn(
        email: String,
        password: String,
        skillsItemUiModel: List<SkillItemUiModel>
    ): Single<AuthUser>

    fun signUp(email: String,
               password: String,
               skillsItemUiModel: List<SkillItemUiModel>): Single<AuthUser>
}