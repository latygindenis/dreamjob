package com.urbanist.template.feature.auth.domain.signin

import com.urbanist.template.feature.auth.domain.entity.AuthUser
import com.urbanist.template.feature.details.presentation.ui.SkillItemUiModel
import io.reactivex.Single

interface SignInUseCase {

    operator fun invoke(
        email: String,
        password: String,
        skillsItemUiModel: List<SkillItemUiModel>
    ): Single<AuthUser>
}