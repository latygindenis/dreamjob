package com.urbanist.template.feature.auth.data.datasource.di

import com.urbanist.template.core.di.scope.FragmentScope
import com.urbanist.template.feature.auth.data.datasource.AuthFBDataSource
import com.urbanist.template.feature.auth.data.datasource.AuthFBDataSourceImpl
import com.urbanist.template.feature.auth.data.datasource.SkillsApi
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
abstract class AuthDataSourceModule {

    @Module
    companion object {

        @Provides
        @FragmentScope
        @JvmStatic
        fun provideApiXyz(retrofit: Retrofit): SkillsApi = retrofit.create(SkillsApi::class.java)
    }

    @Binds
    @FragmentScope
    abstract fun bindFBDataSource(impl: AuthFBDataSourceImpl): AuthFBDataSource
}