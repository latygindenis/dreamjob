package com.urbanist.template.feature.auth.data.datasource

import com.urbanist.template.feature.auth.data.repository.SkillsAndEmail
import io.reactivex.Completable
import retrofit2.http.Body
import retrofit2.http.POST

interface SkillsApi {

    @POST("/skills")
    fun putSkills(@Body skills: SkillsAndEmail) : Completable
}