package com.urbanist.template.feature.auth.data.repository

import com.urbanist.template.feature.auth.data.datasource.AuthFBDataSource
import com.urbanist.template.feature.auth.data.datasource.SkillsApi
import com.urbanist.template.feature.auth.domain.entity.AuthUser
import com.urbanist.template.feature.details.presentation.ui.SkillItemUiModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

data class SkillsAndEmail(
    val email: String,
    val skillsItemUiModel: List<SkillItemUiModel>
)

class AuthRepositoryImpl @Inject constructor(
    private val authFBDataSource: AuthFBDataSource,
    private val skillsApi: SkillsApi
) : AuthRepository {

    override fun signIn(
        email: String,
        password: String,
        skillsItemUiModel: List<SkillItemUiModel>
    ): Single<AuthUser> =
        authFBDataSource.signIn(email, password)
            .flatMap {
                skillsApi.putSkills(SkillsAndEmail(email, skillsItemUiModel))
                    .andThen(Single.just(AuthUser(email)))
            }
            .observeOn(AndroidSchedulers.mainThread())

    override fun signUp(email: String,
                        password: String,
                        skillsItemUiModel: List<SkillItemUiModel>): Single<AuthUser> =
        authFBDataSource.signUp(email, password)
            .flatMap {
                skillsApi.putSkills(SkillsAndEmail(email, skillsItemUiModel))
                    .andThen(Single.just(AuthUser(email)))
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}