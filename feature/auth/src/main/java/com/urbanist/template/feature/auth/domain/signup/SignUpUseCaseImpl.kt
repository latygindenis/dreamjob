package com.urbanist.template.feature.auth.domain.signup

import com.urbanist.template.feature.auth.domain.entity.AuthUser
import com.urbanist.template.feature.auth.data.repository.AuthRepository
import com.urbanist.template.feature.details.presentation.ui.SkillItemUiModel
import io.reactivex.Single
import javax.inject.Inject

class SignUpUseCaseImpl @Inject constructor(
	private val authRepository: AuthRepository
) : SignUpUseCase {

	override operator fun invoke(email: String, password: String, skillsItemUiModel: List<SkillItemUiModel>): Single<AuthUser> =
		authRepository.signUp(email, password, skillsItemUiModel)
}