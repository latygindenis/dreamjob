package com.urbanist.template.feature.auth.domain.signin

import com.urbanist.template.feature.auth.data.repository.AuthRepository
import com.urbanist.template.feature.auth.domain.entity.AuthUser
import com.urbanist.template.feature.details.presentation.ui.SkillItemUiModel
import io.reactivex.Single
import javax.inject.Inject

class SignInUseCaseImpl @Inject constructor(
    private val authRepository: AuthRepository
) : SignInUseCase {

    override operator fun invoke(
        email: String,
        password: String,
        skillsItemUiModel: List<SkillItemUiModel>
    ): Single<AuthUser> =
        authRepository.signIn(
            email,
            password,
            skillsItemUiModel
        )
}