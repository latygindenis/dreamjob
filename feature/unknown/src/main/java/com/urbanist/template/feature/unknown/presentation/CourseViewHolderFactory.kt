package com.urbanist.template.feature.unknown.presentation

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.memebattle.memes.benderadapter.BindingViewHolder
import com.memebattle.memes.benderadapter.ViewHolderFactory
import com.urbanist.template.feature.unknown.R
import com.urbanist.template.feature.unknown.databinding.ItemCourseBinding

class CourseViewHolderFactory(private val onClick: (url: String) -> Unit) : ViewHolderFactory {

    override fun create(parent: ViewGroup): BindingViewHolder<ViewDataBinding> =
        BindingViewHolder<ItemCourseBinding>(parent, R.layout.item_course)
            .apply {
                binding.imageView.setOnClickListener {
                    onClick(binding.uiModel!!.url)
                }
                binding.name.setOnClickListener {
                    onClick(binding.uiModel!!.url)
                }
            }
}