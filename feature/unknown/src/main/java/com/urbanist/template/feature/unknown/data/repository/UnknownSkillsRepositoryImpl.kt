package com.urbanist.template.feature.unknown.data.repository

import com.urbanist.template.feature.unknown.data.datasource.UnknownSkillsDataSource
import com.urbanist.template.feature.unknown.data.entity.Status
import com.urbanist.template.feature.unknown.data.entity.UnknownSkill
import com.urbanist.template.feature.unknown.data.repository.UnknownSkillsRepository
import com.urbanist.template.feature.user.data.datasource.ProfileDataSource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class UnknownSkillsRepositoryImpl @Inject constructor(
    private val unknownSkillsDataSource: UnknownSkillsDataSource,
    private val profileDataSource: ProfileDataSource
) : UnknownSkillsRepository {
    override fun getUnknownSkills(): Single<List<UnknownSkill>> =
        unknownSkillsDataSource.getKnownSkills(profileDataSource.getEmail()!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    override fun completeSkill(skill: String): Single<Status> =
        unknownSkillsDataSource.postCompleteSkill(profileDataSource.getEmail()!!, skill)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}