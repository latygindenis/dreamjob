package com.urbanist.template.feature.unknown.data.datasource

import com.urbanist.template.feature.unknown.data.api.UnknownSkillsApi
import com.urbanist.template.feature.unknown.data.entity.UnknownSkill
import io.reactivex.Single
import javax.inject.Inject

class UnknownSkillsApiDataSorceImpl @Inject constructor(
    private val unknownSkillsApi: UnknownSkillsApi
) : UnknownSkillsDataSource {

    override fun getKnownSkills(email: String): Single<List<UnknownSkill>> =
        unknownSkillsApi.getKnownSkills(email)

    override fun postCompleteSkill(email: String, skill: String) =
        unknownSkillsApi.postCompleteSkill(Complete(email, skill))
}