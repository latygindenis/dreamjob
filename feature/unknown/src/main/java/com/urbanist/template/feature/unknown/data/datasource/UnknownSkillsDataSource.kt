package com.urbanist.template.feature.unknown.data.datasource

import com.urbanist.template.feature.unknown.data.entity.Status
import com.urbanist.template.feature.unknown.data.entity.UnknownSkill
import io.reactivex.Single

interface UnknownSkillsDataSource {

    fun getKnownSkills(email: String): Single<List<UnknownSkill>>
    fun postCompleteSkill(email: String, skill: String): Single<Status>
}