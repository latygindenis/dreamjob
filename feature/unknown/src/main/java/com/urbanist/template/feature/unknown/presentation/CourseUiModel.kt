package com.urbanist.template.feature.unknown.presentation

import androidx.databinding.ViewDataBinding
import com.memebattle.memes.benderadapter.BindingClass
import com.urbanist.template.feature.unknown.R

class CourseUiModel (
    val name: String,
    val url: String
) : BindingClass {

    override val layoutId: Int = R.layout.item_course

    override fun bind(viewDataBinding: ViewDataBinding) {
        viewDataBinding.setVariable(com.urbanist.template.feature.unknown.BR.uiModel, this)
    }
}