package com.urbanist.template.feature.unknown.data.entity

data class UnknownSkill(val name: String, val courses: List<Course>)