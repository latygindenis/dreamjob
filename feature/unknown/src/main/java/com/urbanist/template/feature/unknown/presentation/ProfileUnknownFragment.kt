package com.urbanist.template.feature.unknown.presentation

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.memebattle.memes.benderadapter.BinderAdapter
import com.memebattle.memes.mvvm.fragment.BaseFragment
import com.urbanist.template.feature.unknown.R
import com.urbanist.template.feature.unknown.databinding.FragmentProfileUnknownBinding
import com.urbanist.template.feature.unknown.presentation.navigation.ProfileUnknownNavCommandProvider
import javax.inject.Inject

class ProfileUnknownFragment : BaseFragment<FragmentProfileUnknownBinding>(),
    ProfileUnknownViewModel.EventsListener {

	override val layoutId: Int = R.layout.fragment_profile_unknown

	@Inject
	lateinit var navCommandProvider: ProfileUnknownNavCommandProvider

	@Inject
	lateinit var viewModel: ProfileUnknownViewModel

	private val onCourseClick: (url: String) -> Unit = {
		val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
		startActivity(browserIntent)
	}

	private val onSkillClick: (name: String) -> Unit = {
		viewModel.completeSkill(it)
	}

	private val courseFactory = CourseViewHolderFactory(onCourseClick)
	private val skillFactory = SkillViewHolderFactory(onSkillClick)

	private val adapter = BinderAdapter(
		mapOf(
			R.layout.item_course to courseFactory,
			R.layout.item_unknown_skill to skillFactory
		)
	)

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		val view = super.onCreateView(inflater, container, savedInstanceState)
		viewModel.eventsDispatcher.bind(this, this)
		binding.viewModel = viewModel
		binding.skillsList.adapter = adapter

		val preferences = requireActivity().getSharedPreferences("SELECTED_VACACIES_PREF", Context.MODE_PRIVATE)!!

		val vacancyName = preferences.getString("vacancyName", "")
		val city = preferences.getString("city", "")
		val companyName = preferences.getString("companyName", "")
		val salary = preferences.getString("salary", "")

		binding.vacancyName.text = vacancyName
		binding.vacancyCity.text = city
		binding.vacancyCompany.text = companyName
		binding.vacancySalary.text = salary

		return view
	}
}
