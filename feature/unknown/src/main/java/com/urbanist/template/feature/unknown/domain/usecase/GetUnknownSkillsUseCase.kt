package com.urbanist.template.feature.unknown.domain.usecase

import com.urbanist.template.feature.unknown.data.entity.UnknownSkill
import com.urbanist.template.feature.unknown.data.repository.UnknownSkillsRepository
import io.reactivex.Single
import javax.inject.Inject

class GetUnknownSkillsUseCase @Inject constructor(
    private val knownSkillsRepository: UnknownSkillsRepository
) {

    operator fun invoke(): Single<List<UnknownSkill>> =
        knownSkillsRepository.getUnknownSkills()
}