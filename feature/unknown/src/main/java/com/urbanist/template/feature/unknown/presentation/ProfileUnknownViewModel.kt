package com.urbanist.template.feature.unknown.presentation

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.memebattle.memes.benderadapter.BindingClass
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcher
import com.memebattle.memes.mvvm.eventsdispatcher.EventsDispatcherOwner
import com.memebattle.memes.mvvm.viewmodel.BaseViewModel
import com.memebattle.memes.utils.ext.addTo
import com.urbanist.template.feature.unknown.domain.usecase.CompleteSkillUseCase
import com.urbanist.template.feature.unknown.domain.usecase.GetUnknownSkillsUseCase
import javax.inject.Inject

class ProfileUnknownViewModel @Inject constructor(
    private val getUnknownSkillsUseCase: GetUnknownSkillsUseCase,
    private val completeSkillsUseCase: CompleteSkillUseCase
) : BaseViewModel(), EventsDispatcherOwner<ProfileUnknownViewModel.EventsListener> {

    override val eventsDispatcher: EventsDispatcher<EventsListener> = EventsDispatcher()
    val emptyTextVisibility = MutableLiveData<Int>()

    val skills = MutableLiveData<List<BindingClass>>()

    init {
        emptyTextVisibility.value = View.GONE
        getSkills()
    }

    fun completeSkill(name: String) {
        completeSkillsUseCase(name)
            .subscribe({
                getSkills()
            }, {
                it
            })
            .addTo(compositeDisposable)
    }

    private fun getSkills() {
        getUnknownSkillsUseCase()
            .subscribe({ it ->
                if (it.isEmpty()) {
                    emptyTextVisibility.value = View.VISIBLE
                } else {
                    val unknownSkills = arrayListOf<BindingClass>()
                    it.map {
                        unknownSkills.add(UnknownSkillUiModel(it.name))
                        it.courses.map {
                            unknownSkills.add(CourseUiModel(it.name, it.url))
                        }
                    }
                    skills.value = unknownSkills
                }
            }, { t ->
                t
            })
            .addTo(compositeDisposable)
    }

    interface EventsListener {

    }
}