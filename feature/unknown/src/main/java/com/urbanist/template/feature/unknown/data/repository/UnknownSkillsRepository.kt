package com.urbanist.template.feature.unknown.data.repository

import com.urbanist.template.feature.unknown.data.entity.Status
import com.urbanist.template.feature.unknown.data.entity.UnknownSkill
import io.reactivex.Single

interface UnknownSkillsRepository {

    fun getUnknownSkills(): Single<List<UnknownSkill>>

    fun completeSkill(skill: String): Single<Status>
}