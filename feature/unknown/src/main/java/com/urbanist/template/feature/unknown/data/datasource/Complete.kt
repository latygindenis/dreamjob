package com.urbanist.template.feature.unknown.data.datasource

data class Complete(val email: String, val skill: String)