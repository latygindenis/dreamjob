package com.urbanist.template.feature.unknown.data.api

import com.urbanist.template.feature.unknown.data.datasource.Complete
import com.urbanist.template.feature.unknown.data.entity.Status
import com.urbanist.template.feature.unknown.data.entity.UnknownSkill
import io.reactivex.Single
import retrofit2.http.*

interface UnknownSkillsApi {

    @GET("/profile/unknown")
    fun getKnownSkills(@Query("email") email: String): Single<List<UnknownSkill>>

    @POST("profile/complete")
    fun postCompleteSkill(@Body complete: Complete): Single<Status>
}