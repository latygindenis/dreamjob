package com.urbanist.template.feature.unknown.data.entity

data class Status(val status: String)