package com.urbanist.template.feature.unknown.data.di

import com.urbanist.template.feature.unknown.data.datasource.UnknownSkillsDataSource
import com.urbanist.template.feature.unknown.data.datasource.UnknownSkillsApiDataSorceImpl
import com.urbanist.template.feature.unknown.data.api.di.UnknownSkillsApiModule
import com.urbanist.template.feature.unknown.data.repository.UnknownSkillsRepository
import com.urbanist.template.feature.unknown.data.repository.UnknownSkillsRepositoryImpl
import com.urbanist.template.feature.user.data.datasource.di.ProfileDataSourceModule
import dagger.Binds
import dagger.Module

@Module(includes = [UnknownSkillsApiModule::class, ProfileDataSourceModule::class])
interface UnknownSkillsDataModule {

    @Binds
    fun bindRepository(impl: UnknownSkillsRepositoryImpl): UnknownSkillsRepository

    @Binds
    fun bindDataSorce(impl: UnknownSkillsApiDataSorceImpl): UnknownSkillsDataSource
}