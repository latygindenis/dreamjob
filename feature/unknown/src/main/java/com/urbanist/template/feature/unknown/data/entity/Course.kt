package com.urbanist.template.feature.unknown.data.entity

data class Course(val name: String, val url: String)