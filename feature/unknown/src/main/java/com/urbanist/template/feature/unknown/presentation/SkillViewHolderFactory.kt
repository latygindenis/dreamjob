package com.urbanist.template.feature.unknown.presentation

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.memebattle.memes.benderadapter.BindingViewHolder
import com.memebattle.memes.benderadapter.ViewHolderFactory
import com.urbanist.template.feature.unknown.R
import com.urbanist.template.feature.unknown.databinding.ItemUnknownSkillBinding

class SkillViewHolderFactory(private val onClick: (name: String) -> Unit) : ViewHolderFactory {

    override fun create(parent: ViewGroup): BindingViewHolder<ViewDataBinding> =
        BindingViewHolder<ItemUnknownSkillBinding>(parent, R.layout.item_unknown_skill)
            .apply {
                binding.name.setOnClickListener {
                    onClick(binding.uiModel!!.name)
                }
            }
}