package com.urbanist.template.feature.unknown.domain.usecase

import com.urbanist.template.feature.unknown.data.entity.Status
import com.urbanist.template.feature.unknown.data.repository.UnknownSkillsRepository
import io.reactivex.Single
import javax.inject.Inject

class CompleteSkillUseCase @Inject constructor(
    private val unknownSkillsRepository: UnknownSkillsRepository
) {

    operator fun invoke(skill: String): Single<Status> =
        unknownSkillsRepository.completeSkill(skill)
}