package com.urbanist.template.feature.unknown.data.api.di

import com.urbanist.template.feature.unknown.data.api.UnknownSkillsApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class UnknownSkillsApiModule {

    @Provides
    fun provideApi(retrofit: Retrofit): UnknownSkillsApi =
        retrofit.create(UnknownSkillsApi::class.java)
}