package com.urbanist.template.feature.unknown.presentation.di

import androidx.lifecycle.ViewModelProvider
import com.memebattle.memes.mvvm.viewmodel.ViewModelFactory
import com.urbanist.template.core.di.scope.FragmentScope
import com.urbanist.template.feature.unknown.domain.usecase.CompleteSkillUseCase
import com.urbanist.template.feature.unknown.domain.usecase.GetUnknownSkillsUseCase
import com.urbanist.template.feature.unknown.presentation.ProfileUnknownFragment
import com.urbanist.template.feature.unknown.presentation.ProfileUnknownViewModel
import dagger.Module
import dagger.Provides

@Module
class ProfileUnknownFragmentModule {

	@Provides
	@FragmentScope
	fun provideViewModel(
		owner: ProfileUnknownFragment,
		getUnknownSkillsUseCase: GetUnknownSkillsUseCase,
		completeSkillUseCase: CompleteSkillUseCase
	): ProfileUnknownViewModel = ViewModelFactory {
        ProfileUnknownViewModel(
			getUnknownSkillsUseCase,
			completeSkillUseCase
        )
	}.let { viewModelFactory ->
		ViewModelProvider(owner, viewModelFactory)[ProfileUnknownViewModel::class.java]
	}
}