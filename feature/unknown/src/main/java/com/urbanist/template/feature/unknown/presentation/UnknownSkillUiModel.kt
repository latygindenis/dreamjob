package com.urbanist.template.feature.unknown.presentation

import androidx.databinding.ViewDataBinding
import com.memebattle.memes.benderadapter.BindingClass
import com.urbanist.template.feature.unknown.R

class UnknownSkillUiModel (
    val name: String
) : BindingClass {
    override val layoutId: Int = R.layout.item_unknown_skill

    override fun bind(viewDataBinding: ViewDataBinding) {
        viewDataBinding.setVariable(com.urbanist.template.feature.unknown.BR.uiModel, this)
    }
}