package com.urbanist.template.feature.unknown.data.entity

data class UnknownSkills(val known: List<UnknownSkill>)